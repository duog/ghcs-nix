let
  sources = import ./nix/sources.nix;
  nixpkgs = import sources.nixpkgs {};
in with nixpkgs;

let
  fetchGhc =
    { version, sha256
    , url ? null
    }: fetchTarball {
      url = if url != null then url else "https://downloads.haskell.org/ghc/${version}/ghc-${version}-src.tar.xz";
      inherit sha256;
    };

  fetchGhcGit = { ref, branch ? "master", sha256 }: fetchgit {
      url = "https://gitlab.haskell.org/ghc/ghc";
      rev = ref;
      branchName = branch;
      inherit sha256;
      fetchSubmodules = true;
  };

  buildGhc = {
    version, url ? null, sha256 ? null,
    src ? (fetchGhc { inherit url version sha256; }),
    bootVer, werror ? false, sphinx ? python3Packages.sphinx,
    happy ? haskellPackages.happy,
    alex ? haskellPackages.alex
  }:
    callPackage ./ghc.nix {
      inherit version src;
      bootPkgs = haskell.packages."${bootVer}";
      buildLlvmPackages = llvmPackages;
      inherit werror sphinx happy alex;
    };

  buildGhcHadrian = {
    version, url ? null, sha256 ? null,
    src ? (fetchGhc { inherit url version sha256; }),
    bootVer, werror ? false, sphinx ? python3Packages.sphinx,
    shakeVersion,
    happy ? haskellPackages.happy,
    alex ? haskellPackages.alex,
    libffi ? pkgs.libffi,
  }: callPackage ./hadrian.nix {
    inherit version src;
    buildLlvmPackages = llvmPackages;
    bootGhc = bootVer;
    inherit sphinx libffi alex happy shakeVersion;
  };

  happy-1_20_0 =
    let src = fetchTarball {
      url = "http://hackage.haskell.org/package/happy-1.20.0/happy-1.20.0.tar.gz";
      sha256 = "sha256:095qp013kkppgy2vw0vi3hkahbdif3mas3ryb06psn2vbqd5b47l";
    };
    in haskellPackages.callCabal2nix "happy" src {};

  alex-3_2_6 =
    let src = fetchTarball {
      url = "http://hackage.haskell.org/package/alex-3.2.6/alex-3.2.6.tar.gz";
      sha256 = "sha256:17vnxwbzsp82mg3174c4pyp68ghfbr0dyrw61cmjjqdx32l93r77";
    };
    in haskellPackages.callCabal2nix "alex" src {};

  ghcs = {
    #
    # GHC 8.2
    #
    "8_2_1" = buildGhc {
      version = "8.2.1";
      sha256 = "sha256:12lwazpfxiv7c6rjiz54w8wrc9fdjziiy9vvdm9g31wfdgazmaih";
      bootVer = "ghc822Binary";
      sphinx = null; # Sphinx incompatible
    };
    "8.2.2" = buildGhc {
      version = "8.2.2";
      sha256 = "sha256:10l49pyha8gslvdl7v2kb879knqynnsqa4r9d2cysdqw6i6lcal5";
      bootVer = "ghc822Binary";
      sphinx = null; # Sphinx incompatible
    };

    #
    # GHC 8.4
    #
    "8.4.1" = buildGhc {
      version = "8.4.1";
      sha256 = "sha256:1i8p2l654pm1qsjg8z01mm29rhyqqzx2klm3daqwspql5b3chnna";
      bootVer = "ghc822Binary";
      sphinx = null; # Sphinx incompatible
    };
    "8.4.2" = buildGhc {
      version = "8.4.2";
      sha256 = "sha256:0d19cq7rmrbnv0dabxgcf9gadjas3f02wvighdfgr6zqr1z5fcrc";
      bootVer = "ghc822Binary";
      sphinx = null; # Sphinx incompatible
    };
    "8.4.3" = buildGhc {
      version = "8.4.3";
      sha256 = "sha256:1y8bd6qxi5azqwyr930val428r2yi9igfprv11acd02g7d766yxq";
      bootVer = "ghc822Binary";
      sphinx = null; # Sphinx incompatible
    };

    #
    # GHC 8.6
    #
    "8.6.2" = buildGhc {
      version = "8.6.2";
      sha256 = "sha256:1spb0jfxv3r0f12z6ys1y1ssnqwnnqavr947gnz7a74y6k8kb2h2";
      bootVer = "ghc865";
    };
    "8.6.3" = buildGhc {
      version = "8.6.4";
      sha256 = "sha256:1iz7lsw12vkl6da7cqbxiy5hidkfp3427fm30qv3294pvx2c2szr";
      bootVer = "ghc865";
    };
    "8.6.4" = buildGhc {
      version = "8.6.4";
      sha256 = "sha256:1iz7lsw12vkl6da7cqbxiy5hidkfp3427fm30qv3294pvx2c2szr";
      bootVer = "ghc865";
    };
    "8.6.5" = buildGhc {
      version = "8.6.5";
      sha256 = "sha256:0p7ykswxid024aqq0aqd91yla719kc1rnb5f90ply43xk9457687";
      bootVer = "ghc865";
    };

    #
    # GHC 8.8
    #
    "8.8.1" = buildGhc {
      version = "8.8.1";
      sha256 = "sha256:07ag4ah5dd65l6kxrrx1k9zxw0fswy2ias5kqs61rxif00a982jb";
      bootVer = "ghc865";
    };
    "8.8.2" = buildGhc {
      version = "8.8.2";
      sha256 = "sha256:1qf4nrrxn0fnfvxri5m34nx6f3x2px85xhxnypapa4j3y3dh5xvj";
      bootVer = "ghc865";
    };
    "8.8.3" = buildGhc {
      version = "8.8.3";
      sha256 = "sha256:1xmdrr6cj4i14ginfnprkrj5fh6d621r70fb93m8a72hk6ca9alg";
      bootVer = "ghc865";
    };
    "8.8.4" = buildGhc {
      version = "8.8.4";
      sha256 = "sha256:1vbgjxpl7s4nyfzmdzxy7zcpdzlhnn1zalb497p2w7bf0qamzmdq";
      bootVer = "ghc865";
    };

    #
    # GHC 8.10
    #
    "8.10.1" = buildGhc {
      version = "8.10.1";
      sha256 = "sha256:1jhs396lww687121mdvb723n8ql1rv60x9s3xxy7c9h749gy1n75";
      bootVer = "ghc884";
    };
    # 8.10.2 is currently broken
    #"8.10.2" = buildGhc {
    #  version = "8.10.2";
    #  sha256 = "sha256:03g5k48cm8758fz4y3yv14r5p46cxcspjwy5flr6b1s49ciy2s79";
    #  bootVer = "ghc884";
    #};
    "8.10.3" = buildGhc {
      version = "8.10.3";
      sha256 = "sha256:0phgyrmmnni6cpzah7pgx9plql84bc24c4yhrjvcw2ksk3lncx0g";
      bootVer = "ghc884";
    };
    "8.10.4" = buildGhc {
      version = "8.10.4";
      sha256 = "sha256:1blpmp67i0lb1pcmwg8lpf6cg3mpim3adzg8fjmrvil8yr4wp1w9";
      bootVer = "ghc884";
    };
    "8.10.5" = buildGhc {
      version = "8.10.5";
      sha256 = "sha256:13517aqvrkkpvdk94g7x3q72p2sbjh4qch2yx204zd6prb51hlk1";
      bootVer = "ghc884";
    };
    "8.10.6" = buildGhc {
      version = "8.10.6";
      sha256 = "sha256:00ccsqm4hrb1ffqxib8c69wyj11b1f71yhhhkkhj57cnahfxp7ii";
      bootVer = "ghc884";
    };
    "8.10.7" = buildGhc {
      version = "8.10.7";
      sha256 = "sha256:1n467cknqhm07b471j3hsdpz5cgny5552ydf3kspk7mid1bapyfi";
      bootVer = "ghc884";
    };

    #
    # GHC 9.0
    #
    "9.0.1" = buildGhc {
      version = "9.0.1";
      url = "https://downloads.haskell.org/ghc/9.0.1/ghc-9.0.1-src.tar.xz";
      sha256 = "sha256:1wp240i99gccs21agjq0249lv6raffblzqkl4vpnlj290dycwb9h";
      bootVer = "ghc8101";
    };

    "9.0.2" = buildGhc {
      version = "9.0.2";
      url = "https://downloads.haskell.org/ghc/9.0.2/ghc-9.0.2-src.tar.xz";
      sha256 = "sha256:19647slxmbx1rzz61kfsd0cjixp076rz3bd3c89dhixz41xj469c";
      bootVer = "ghc8101";
    };

    #
    # GHC 9.2
    #
    "9.2.1-rc1" = buildGhc {
      version = "9.2.1-rc1";
      url = "https://downloads.haskell.org/ghc/9.2.1-rc1/ghc-9.2.0.20210821-src.tar.xz";
      sha256 = "sha256:1xmkaa58l67lml2f34bvzcckilqr8blqyhs1b0hpkngh26w8q482";
      bootVer = "ghc8101";
    };

    "9.2.1" = buildGhc {
      version = "9.2.1";
      url = "https://downloads.haskell.org/ghc/9.2.1/ghc-9.2.1-src.tar.xz";
      sha256 = "sha256:1ag4ymx9x54hib7vbwp1z6gz6j88lnq9zjia7x78z8r0jk2wjd7l";
      bootVer = "ghc8101";
      happy = happy-1_20_0;
      alex = alex-3_2_6;
    };

    "9.2.2" = buildGhcHadrian {
      version = "9.2.2";
      url = "https://downloads.haskell.org/ghc/9.2.2/ghc-9.2.2-src.tar.xz";
      sha256 = "sha256:0vvqm2j26am9568z345jracp4gk8x33cz7mbp18zfwagqxizc3yf";
      bootVer = "ghc8101";
      happy = happy-1_20_0;
      alex = alex-3_2_6;
    };

    #
    # master
    #
    "master" = buildGhc {
      version = "9.3.0";
      src = sources.ghc-master;
      sphinx = python3Packages.sphinx;
      bootVer = "ghc8101";
      happy = happy-1_20_0;
      alex = alex-3_2_6;
    };
  };

  #
  # Experimental Hadrian derivations
  #
  ghcs_hadrian = {
    #
    # GHC 9.0
    #
    # N.B. hadrian's installation support is broken in 9.0.1.

    #"9.0.1" = buildGhcHadrian {
    #  version = "9.0.1";
    #  sha256 = "sha256:1wp240i99gccs21agjq0249lv6raffblzqkl4vpnlj290dycwb9h";
    #  bootVer = "ghc8101";
    #  libffi = null; # FIXME: hadrian's with-system-libffi support is broken
    #};

    #
    # GHC 9.2 (experimental Hadrian derivation)
    #
    "9.2.1-rc1" = buildGhcHadrian {
      version = "9.2.1-rc1";
      url = "https://downloads.haskell.org/ghc/9.2.1-rc1/ghc-9.2.0.20210821-src.tar.xz";
      sha256 = "sha256:1xmkaa58l67lml2f34bvzcckilqr8blqyhs1b0hpkngh26w8q482";
      bootVer = "ghc8101";
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_6;
      shakeVersion = "0.18.5";
    };

    "9.2.1" = buildGhcHadrian {
      version = "9.2.1";
      url = "https://downloads.haskell.org/ghc/9.2.1/ghc-9.2.1-src.tar.xz";
      sha256 = "sha256:1ag4ymx9x54hib7vbwp1z6gz6j88lnq9zjia7x78z8r0jk2wjd7l";
      bootVer = "ghc8101";
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_6;
      shakeVersion = "0.18.5";
    };

    "9.2.2" = buildGhcHadrian {
      version = "9.2.2";
      url = "https://downloads.haskell.org/ghc/9.2.2/ghc-9.2.2-src.tar.xz";
      sha256 = "sha256:0vvqm2j26am9568z345jracp4gk8x33cz7mbp18zfwagqxizc3yf";
      bootVer = "ghc8101";
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_6;
      shakeVersion = "0.18.5";
    };

    #
    # GHC 9.4
    #
    "9.4.1-alpha1" = buildGhcHadrian {
      version = "9.4.1-alpha1";
      url = "https://downloads.haskell.org/ghc/9.4.1-alpha1/ghc-9.4.0.20220501-src.tar.xz";
      sha256 = "sha256:042wllyi6wbv9zyynjiak3vvky5k12brf7f9yyl7zb0dvgdaa98w";
      bootVer = "ghc921";
      libffi = null; # FIXME: hadrian's with-system-libffi support is broken
      happy = happy-1_20_0;
      alex = alex-3_2_6;
      shakeVersion = "0.19.6";
    };
  };

  #
  # Cabal-install
  #
  buildCabal = callPackage ./cabal-install.nix {} ghcs;

  cabals = {
    "3.4.0.0" = buildCabal { version = "cabal-install-3.4.0.0";
                sha256 = "0q32issgbg57rv60rriacqw5bsrxsskb9v1xbm6py72piyy2lq6k";
                bootVer = "8.10.1";
              };
    "3.6.2.0" = buildCabal { version = "Cabal-v3.6.2.0";
                sha256 = "04sbn42wxwx46b6vg658nqmjbv23lr5y8mavhhiwp0s3kjlkjj3a";
                bootVer = "8.10.7";
              };
    };


  #
  # Administrivia
  #
  join =
    let
      wrapGhc = version: drv: "ln -s ${drv}/bin/ghc $out/bin/ghc-${version}";
    in runCommand "all-ghcs" {
      } ''
        mkdir -p $out/bin
        ${lib.concatStringsSep "\n" (lib.mapAttrsToList wrapGhc ghcs)}
      '';

  mapNames = f: attrset:
    lib.mapAttrs' (name: value: lib.nameValuePair (f name) value) attrset;
  underscoredVersion = version: lib.replaceStrings ["."] ["_"] version;


  attrs =
    (mapNames (v: "cabal-install-" + underscoredVersion v) cabals)
    // (mapNames (v: "ghc-" + underscoredVersion v) ghcs)
    // (mapNames (v: "ghc-hadrian-" + underscoredVersion v) ghcs_hadrian);

  cabal-install = attrs.cabal-install-3_6_2_0;

in
  {
    inherit nixpkgs join cabal-install ghcs ghcs_hadrian;

    # derivations to be built by CI
    ciDrvs = attrs;
  } // attrs
